<?php

/**
 * Generates the settings form for orders.
 *
 * @ingroup forms
 */
function ucx_invoice_settings_form($form, &$form_state){
  // Put fieldsets into vertical tabs
  $form['invoice-settings'] = array('#type' => 'vertical_tabs');
  $form['invoice'] = array(
    '#type' => 'text',
  );  
  

  return system_settings_form($form);  
}